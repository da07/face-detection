package main

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"log"
	"time"

	"github.com/machinebox/sdk-go/facebox"

	"gocv.io/x/gocv"
)

var (
	faceAlgorithm = "haarcascade_frontface_default.xml"
	blue          = color.RGBA{0, 0, 255, 0}
	fbox          = facebox.New("https://localhost:8080")
)

func main() {
	webcam, err := gocv.VideoCaptureDevice(0)
	if err != nil {
		log.Fatal("unable to init web cam : %v", err)
	}
	defer webcam.Close()

	img := gocv.NewMat()
	defer img.Close()

	window := gocv.NewWindow("face detection in Go")
	defer window.Close()
	classifier := gocv.NewCascadeClassifier()
	classifier.Load(faceAlgorithm)
	defer classifier.Close()

	for {
		if ok := webcam.Read(&img); !ok || img.Empty() {
			log.Printf("unable to read from web cam")
			continue
		}

		rects := classifier.DetectMultiScale(img)
		for _, r := range rects {
			imgFace := img.Region(r)
			gocv.IMWrite(fmt.Sprintf("%d.jpg", time.Now().UnixNano()), imgFace)
			buf, err := gocv.IMEncode(".jpg", imgFace)
			if err != nil {
				log.Printf("unable to encode face img :%v", err)
				continue
			}
			imgFace.Close()

			faces, err := fbox.Check(bytes.NewReader(buf))
			if err != nil {
				log.Printf("unable to check face %v", err)
				continue
			}

			text := "I don't Know You"
			if len(faces) > 0 {
				text = faces[0].Name
			}
			size := gocv.GetTextSize(text, gocv.FontHersheyPlain, 3, 2)
			pt := image.Pt(r.Min.X+(r.Min.X/2)-(size.X/2), r.Min.Y-2)
			gocv.PutText(&img, text, pt, gocv.FontHersheyPlain, 3, blue, 2)
			gocv.Rectangle(&img, r, blue, 2)
		}
		window.IMShow(img)
		window.WaitKey(500)
	}
}
